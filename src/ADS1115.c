/*
 * ADS1115.c
 *
 *  Created on: 28/02/2017
 *      Author: Cesar
 */

#include "main.h"
#include "FreeRTOS.h"
#include "task.h"

STATIC void I2C_Setting( I2C_ID_T i2c, uint32_t clockrate )
{
	/*		Setup I2C		*/
	Board_I2C_Init(i2c);
	Chip_I2C_Init(i2c);
	Chip_I2C_SetClockRate(i2c, clockrate);
	Chip_I2C_SetMasterEventHandler(i2c, Chip_I2C_EventHandlerPolling);
}


STATIC INLINE void setAddressPointer( ADDR_POINTER_REG_T addrPointer )
{
	const uint8_t wbuf = (uint8_t)addrPointer;

	Chip_I2C_MasterSend(I2C1, ADS1115_ADDR, &wbuf, 1);
}


void ADS1115_Init( void )
{
	I2C_Setting(I2C1, 400000); /* set i2c_1 clock = 400KHz */
}


void ADS1115_SetConfigRegister( OPERATING_MODE_T mode )
{
	const uint8_t wbuf[3] = {(uint8_t)CONFIG_REG, (mode >> 8), (mode & 0xFF)};

	Chip_I2C_MasterSend(I2C1, ADS1115_ADDR, wbuf, 3);
}


Status ADS1115_ReadConversion( uint32_t n_muestras, uint16_t* dato )
{
	//Creo una variable para almacenar el dato concatenado
	uint16_t dato_final = 65535;
	uint32_t suma = 0;
	uint32_t muestras;
	//Creo las variables para almacenar los 2 datos de 8 bits cada uno que llegaran por I2C
	uint8_t rbuf[2] = {0};

	muestras = n_muestras;

	setAddressPointer(CONVERSION_REG);

	while(muestras > 0) {
		Chip_I2C_MasterRead(I2C1, ADS1115_ADDR, rbuf, 2);
		muestras--;

		//Concateno los 2 Bytes que llegaron por I2C
		dato_final = (rbuf[0]<<8)|rbuf[1];

		//Hago la suma para luego realizar el promedio
		suma = suma + dato_final;
		vTaskDelay(20 / portTICK_RATE_MS);
	}
	//Hago la division para obtener el valor promediado y luego lo asigno al valor que entrego
	*dato = suma/ n_muestras;
	return SUCCESS;
}


uint16_t ADS1115_GetConfigRegister( void )
{
	uint16_t config = 0;
	uint8_t rbuf[2] = {0};
	setAddressPointer(CONFIG_REG);

	Chip_I2C_MasterRead(I2C1, ADS1115_ADDR, rbuf, 2);

	config = rbuf[0] << 8;
	config |= rbuf[1];

	return config;
}


uint8_t ADS1115_GeneralCall( void )
{
	uint8_t reset = 0;
	Chip_I2C_MasterSend(I2C1, 0x00, &reset, 1);

	return reset;
}
